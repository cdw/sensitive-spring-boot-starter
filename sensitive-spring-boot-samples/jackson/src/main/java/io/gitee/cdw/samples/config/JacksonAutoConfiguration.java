package io.gitee.cdw.samples.config;

import io.gitee.cdw.samples.util.SensitiveUtil;
import io.gitee.cdw.sensitive.strategy.StrategyManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * @author Created by chendw on 2023/5/11 12:11.
 */
@Configuration(proxyBeanMethods = false)
public class JacksonAutoConfiguration {

    @Autowired
    public void sensitiveStrategyLoader(StrategyManager strategyManager) {
        // 模拟从数据库读取脱敏规则
        SensitiveUtil.initConfig(strategyManager);
    }
}
