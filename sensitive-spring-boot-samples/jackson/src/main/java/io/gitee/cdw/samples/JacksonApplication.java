package io.gitee.cdw.samples;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksonApplication {


    public static void main(String[] args) {
        SpringApplication.run(JacksonApplication.class, args);
    }

}
