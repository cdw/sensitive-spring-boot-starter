package io.gitee.cdw.samples.web;

import io.gitee.cdw.samples.model.SensitiveInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created by chendw on 2023/5/10 10:34.
 */
@RestController
public class SensitiveController {

    @RequestMapping("/sensitive")
    public SensitiveInfo sensitive() {
        SensitiveInfo sensitiveInfo = new SensitiveInfo();
        sensitiveInfo.setId("000000");
        sensitiveInfo.setName("王老五");
        sensitiveInfo.setPhone("13611112400");
        sensitiveInfo.setWorkPhone("18011111068");
        sensitiveInfo.setIdCard("330327111111111994");
        sensitiveInfo.setCardNo("123456789");
        sensitiveInfo.setAge(30);
        sensitiveInfo.setPassword("123456");
        sensitiveInfo.setAddress("滨江区越达巷XX号");
        sensitiveInfo.getList().add("one");
        return sensitiveInfo;
    }
}
