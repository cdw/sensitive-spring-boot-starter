package io.gitee.cdw.samples.util;

import io.gitee.cdw.sensitive.strategy.StrategyManager;

/**
 * @author Created by chendw on 2023/5/10 17:56.
 */
public class SensitiveUtil {

    private SensitiveUtil() {
    }

    public static void initConfig(StrategyManager strategyManager) {
        strategyManager.addConfig("io.gitee.cdw.samples.model.TestParentEntity#id", "custom", 2, 2);
        strategyManager.addConfig("io.gitee.cdw.samples.model.SensitiveInfo#phone", "mobile");
        strategyManager.addConfig("io.gitee.cdw.samples.model.SensitiveInfo#workPhone", "mobile", 0, 4);
        strategyManager.addConfig("io.gitee.cdw.samples.model.SensitiveInfo#idCard", "idCard");
        strategyManager.addConfig("io.gitee.cdw.samples.model.SensitiveInfo#password", "all");

        strategyManager.addPatternConfig("io.gitee.cdw.samples.model.SensitiveInfo#cardNo", "(?<=\\w{3})\\w(?=\\w{4})");
        strategyManager.reload();
    }
}
