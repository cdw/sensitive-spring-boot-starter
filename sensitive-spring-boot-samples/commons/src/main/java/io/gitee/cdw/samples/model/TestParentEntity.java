package io.gitee.cdw.samples.model;

import lombok.Data;

/**
 * @author Created by chendw on 2023/5/6 9:21.
 */
@Data
public class TestParentEntity {
    private String id;

}
