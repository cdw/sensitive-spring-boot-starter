package io.gitee.cdw.samples;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastjonApplication {


    public static void main(String[] args) {
        SpringApplication.run(FastjonApplication.class, args);
    }

}
