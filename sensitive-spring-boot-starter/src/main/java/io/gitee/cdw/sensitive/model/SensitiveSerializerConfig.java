package io.gitee.cdw.sensitive.model;

import io.gitee.cdw.sensitive.strategy.IStrategy;
import lombok.Data;

/**
 * 脱敏序列化器参数
 *
 * @author Created by chendw on 2023/5/6 15:23.
 */
@Data
public class SensitiveSerializerConfig {

    /**
     * 脱敏策略
     */
    private IStrategy strategy;

    /**
     * 脱敏策略参数
     */
    private StrategyConfig config;

    @Override
    public String toString() {
        return "SensitiveSerializerConfig{" +
                "strategy=" + strategy.id() +
                ", config=" + config +
                '}';
    }
}
