package io.gitee.cdw.sensitive;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * 需要通过Jackson进行数据脱敏的ObjectMapper加载接口类
 *
 * @author Created by chendw
 */
public interface IJackson2ObjectMapperLoader {

    /**
     * 获取需要进行数据脱敏的Jackson ObjectMapper
     *
     * @return 需要进行脱敏的 ObjectMapper 对象
     */
    List<ObjectMapper> get();

}
