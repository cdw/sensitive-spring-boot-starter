package io.gitee.cdw.sensitive.autoconfigure;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import io.gitee.cdw.sensitive.serializer.FastjsonSensitiveValueFilter;
import io.gitee.cdw.sensitive.serializer.SensitiveSerializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author Created by chendw on 2024/1/19 12:52.
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(FastJsonHttpMessageConverter.class)
public class SensitiveFastjsonConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public FastjsonSensitiveValueFilter fastjsonSensitiveValueFilter(SensitiveSerializer sensitiveSerializer) {
        return new FastjsonSensitiveValueFilter(sensitiveSerializer);
    }

    @Bean
    public WebMvcConfigurer webMvcConfigurer(@Autowired(required = false) FastjsonSensitiveValueFilter fastjsonSensitiveValueFilter) {
        return new WebMvcConfigurer() {
            @Override
            public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
                for (HttpMessageConverter<?> converter : converters) {
                    if (converter instanceof FastJsonHttpMessageConverter) {
                        ((FastJsonHttpMessageConverter) converter).getFastJsonConfig().setSerializeFilters(fastjsonSensitiveValueFilter);
                        log.debug("Init FastJsonHttpMessageConverter add fastjsonSensitiveValueFilter");
                        return;
                    }
                }
            }
        };
    }
}
