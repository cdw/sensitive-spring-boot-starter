package io.gitee.cdw.sensitive.model;

/**
 * @author Created by chendw on 2023/5/9 17:25.
 */
public class SensitiveConst {

    private SensitiveConst() {

    }

    public static final String SENSITIVE_KEY = "Sensitive-Api-Name";

    public static final String SENSITIVE_STRATEGY_PASSWORD = "password";
    public static final String SENSITIVE_STRATEGY_IDCARD = "idCard";

    public static final String SENSITIVE_STRATEGY_MOBILE = "mobile";

    public static final String SENSITIVE_STRATEGY_EMAIL = "email";

    public static final String SENSITIVE_STRATEGY_CUSTOM = "custom";
    public static final String SENSITIVE_STRATEGY_ALL = "all";
    public static final String SENSITIVE_MASK_CHAR = "*";
}
