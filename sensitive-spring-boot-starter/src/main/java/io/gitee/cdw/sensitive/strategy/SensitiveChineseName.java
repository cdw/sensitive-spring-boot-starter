package io.gitee.cdw.sensitive.strategy;

import io.gitee.cdw.sensitive.util.SensitiveInfoUtils;
import org.springframework.stereotype.Component;

/**
 * @author Created by chendw on 2023/5/6 17:26.
 */
@Component
public class SensitiveChineseName implements IStrategy {

    @Override
    public String id() {
        return "chineseName";
    }

    @Override
    public String desensitization(String source, int left, int right) {
        if (left > 0 || right > 0) {
            return SensitiveInfoUtils.desValue(source, left, right, "*");
        }
        return SensitiveInfoUtils.chineseName(source);
    }


}