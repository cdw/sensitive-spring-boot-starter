package io.gitee.cdw.sensitive.strategy;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import static io.gitee.cdw.sensitive.model.SensitiveConst.SENSITIVE_MASK_CHAR;
import static io.gitee.cdw.sensitive.model.SensitiveConst.SENSITIVE_STRATEGY_ALL;

/**
 * @author Created by chendw on 2023/5/6 14:36.
 */
@Component
public class SensitiveAll implements IStrategy {

    @Override
    public String id() {
        return SENSITIVE_STRATEGY_ALL;
    }

    @Override
    public String desensitization(String source, int left, int right) {
        return StringUtils.repeat(SENSITIVE_MASK_CHAR, 6);
    }
}