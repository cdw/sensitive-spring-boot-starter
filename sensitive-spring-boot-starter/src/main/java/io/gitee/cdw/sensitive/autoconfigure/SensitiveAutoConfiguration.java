package io.gitee.cdw.sensitive.autoconfigure;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitee.cdw.sensitive.IJackson2ObjectMapperLoader;
import io.gitee.cdw.sensitive.strategy.StrategyManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.List;

/**
 * 数据脱敏的工具类
 *
 * @author Created by chendw on 2023/5/8 15:42.
 */
@Slf4j
@Configuration
@ComponentScan("io.gitee.cdw.sensitive")
@Import({SensitiveFastjsonConfiguration.class, SensitiveJackson2Configuration.class})
public class SensitiveAutoConfiguration {

    @Bean
    public StrategyManager strategyManager() {
        return new StrategyManager();
    }

    @Bean
    @ConditionalOnMissingBean
    public IJackson2ObjectMapperLoader objectMapperLoader(List<ObjectMapper> objectMappers) {
        return () -> objectMappers;
    }

}
