package io.gitee.cdw.sensitive.strategy;

import io.gitee.cdw.sensitive.util.SensitiveInfoUtils;
import org.springframework.stereotype.Component;

import static io.gitee.cdw.sensitive.model.SensitiveConst.SENSITIVE_MASK_CHAR;
import static io.gitee.cdw.sensitive.model.SensitiveConst.SENSITIVE_STRATEGY_MOBILE;

/**
 * @author Created by chendw on 2023/5/6 14:36.
 */
@Component
public class SensitiveMobile implements IStrategy {

    @Override
    public String id() {
        return SENSITIVE_STRATEGY_MOBILE;
    }

    @Override
    public String desensitization(String mobile, int left, int right) {
        if (left > 0 || right > 0) {
            return SensitiveInfoUtils.desValue(mobile, left, right, SENSITIVE_MASK_CHAR);
        }
        return SensitiveInfoUtils.mobilePhone(mobile);
    }
}