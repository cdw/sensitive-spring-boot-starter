package io.gitee.cdw.sensitive.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 敏感字段自定义序列化类
 *
 * @author Created by chendw on 2023/5/6 9:10.
 */
@Slf4j
public class JacksonSensitiveSerializer extends JsonSerializer<Object> {

    private final String field;
    private final SensitiveSerializer serializer;

    protected JacksonSensitiveSerializer(String field, SensitiveSerializer serializer) {
        this.field = field;
        this.serializer = serializer;
    }

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        gen.writeString((String) serializer.serialize(field, value));
    }
}
