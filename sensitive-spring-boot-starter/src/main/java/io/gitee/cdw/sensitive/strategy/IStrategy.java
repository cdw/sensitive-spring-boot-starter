package io.gitee.cdw.sensitive.strategy;

import io.gitee.cdw.sensitive.util.SensitiveInfoUtils;

/**
 * 脱敏策略
 *
 * @author Created by chendw on 2023/5/6 9:10.
 */
public interface IStrategy {

    /**
     * 获取脱敏策略ID全局唯一
     **/
    String id();

    /**
     * 脱敏的具体实现方法
     *
     * @param source 原来对象属性
     * @param left   左边显示明文位数
     * @param right  右边显示明文位数
     * @return 返回脱敏后的信息
     */
    String desensitization(final String source, int left, int right);

    /**
     * 脱敏的具体实现方法
     *
     * @param source      原来对象属性
     * @param pattern     内容显示正则
     * @param replaceChar 替换后的字符
     * @return 返回脱敏后的信息
     */
    default String desensitizationByPattern(String source, String pattern, String replaceChar) {
        return SensitiveInfoUtils.patternReplace(source, pattern, replaceChar);
    }
}
