package io.gitee.cdw.sensitive.model;

import lombok.Data;

/**
 * 脱敏策略参数
 *
 * @author Created by chendw on 2023/5/6 16:57.
 */
@Data
public class StrategyConfig {
    /**
     * 接口名称
     */
    private String name;
    /**
     * 开始显示的字符长度
     */
    private int left;

    /**
     * 结尾显示的字符长度
     */
    private int right;

    /**
     * 脱敏的正则
     */
    private String pattern;
}
