package io.gitee.cdw.sensitive.strategy.loader;

import io.gitee.cdw.sensitive.model.FieldStrategyConfig;

import java.util.List;

/**
 * 脱敏策略加载器
 *
 * @author Created by chendw on 2023/5/8 11:08.
 */
public interface ISensitiveStrategyLoader {
    /**
     * 加载脱敏策略
     */
    List<FieldStrategyConfig> load();

}
