package io.gitee.cdw.sensitive.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 字段脱敏设置
 *
 * @author Created by chendw on 2023/5/6 16:57.
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class FieldStrategyConfig extends StrategyConfig {

    private String field;

    private String type;

}
