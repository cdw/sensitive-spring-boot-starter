package io.gitee.cdw.sensitive.strategy;

import io.gitee.cdw.sensitive.util.SensitiveInfoUtils;
import org.springframework.stereotype.Component;

import static io.gitee.cdw.sensitive.model.SensitiveConst.SENSITIVE_MASK_CHAR;
import static io.gitee.cdw.sensitive.model.SensitiveConst.SENSITIVE_STRATEGY_CUSTOM;

/**
 * @author Created by chendw on 2023/5/6 14:36.
 */
@Component
public class SensitiveCustom implements IStrategy {

    @Override
    public String id() {
        return SENSITIVE_STRATEGY_CUSTOM;
    }

    @Override
    public String desensitization(String source, int left, int right) {
        return SensitiveInfoUtils.desValue(source, left, right, SENSITIVE_MASK_CHAR);
    }
}