package org.boot.sensitive.strategy.loader;

import org.boot.sensitive.model.FieldStrategyConfig;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试策略加载器
 *
 * @author Created by chendw on 2023/5/8 14:36.
 */
@Component
public class CustomSensitiveStrategyLoader implements ISensitiveStrategyLoader {
    @Override
    public List<FieldStrategyConfig> load() {
        List<FieldStrategyConfig> list = new ArrayList<>();

        FieldStrategyConfig config = new FieldStrategyConfig();
        config.setField("org.boot.sensitive.serializer.TestEntity#address");
        config.setType("custom");
        config.setLeft(6);

        list.add(config);
        return list;
    }
}
