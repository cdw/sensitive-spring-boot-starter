package org.boot.sensitive.util;

import org.boot.sensitive.strategy.StrategyManager;

/**
 * @author Created by chendw on 2023/5/10 17:56.
 */
public class SensitiveTestUtil {

    private SensitiveTestUtil() {
    }

    public static void initConfig(StrategyManager strategyManager) {
        strategyManager.addConfig("org.boot.sensitive.serializer.TestParentEntity#id", "custom", 2, 2);
        strategyManager.addConfig("org.boot.sensitive.serializer.TestEntity#phone", "mobile");
        strategyManager.addConfig("org.boot.sensitive.serializer.TestEntity#workPhone", "mobile", 0, 4);
        strategyManager.addConfig("org.boot.sensitive.serializer.TestEntity#idCard", "idCard");
        strategyManager.addConfig("org.boot.sensitive.serializer.TestEntity#password", "all");

        strategyManager.addPatternConfig("org.boot.sensitive.serializer.TestEntity#cardNo", "(?<=\\w{3})\\w(?=\\w{4})");
        strategyManager.reload();
    }
}
