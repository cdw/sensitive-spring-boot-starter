package org.boot.sensitive.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class SensitiveInfoUtilsTest {

    @Test
    void chineseName() {
        String[] names = new String[]{"张三", "王老五"};
        String[] results = new String[]{"*三", "王*五"};
        int idx = 0;
        for (String name : names) {
            String result = SensitiveInfoUtils.chineseName(name);
            log.info("脱敏结果: {}", result);
            assertEquals(result, results[idx++]);
        }
    }
}