package org.boot.sensitive.util;

import org.boot.sensitive.serializer.TestEntity;

/**
 * @author Created by chendw on 2023/5/9 17:35.
 */
public class DataUtils {
    public static TestEntity getEntity() {
        TestEntity testEntity = new TestEntity();
        testEntity.setId("000000");
        testEntity.setName("王老五");
        testEntity.setPhone("13611112400");
        testEntity.setWorkPhone("18011111068");
        testEntity.setIdCard("330327196403071994");
        testEntity.setCardNo("123456789");
        testEntity.setAge(30);
        testEntity.setPassword("123456");
        testEntity.setAddress("滨江区越达巷XX号");
        testEntity.getList().add("one");
        return testEntity;
    }
}
