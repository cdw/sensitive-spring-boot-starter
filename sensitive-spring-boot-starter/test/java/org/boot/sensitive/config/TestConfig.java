package org.boot.sensitive.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.boot.sensitive.ISensitiveObjectMapperLoader;
import org.boot.sensitive.strategy.IStrategy;
import org.boot.sensitive.strategy.StrategyManager;
import org.boot.sensitive.strategy.loader.ISensitiveStrategyLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;

import java.util.List;

/**
 * @author Created by chendw on 2023/5/9 17:13.
 */
@Configuration
public class TestConfig {

    @Bean
    public ISensitiveObjectMapperLoader objectMapperLoader(List<ObjectMapper> objectMappers) {
        return () -> objectMappers;
    }

    @Bean
    public StrategyManager strategyManager() {
        return new StrategyManager();
    }


}
