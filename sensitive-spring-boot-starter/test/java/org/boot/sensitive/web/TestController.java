package org.boot.sensitive.web;

import org.boot.sensitive.serializer.TestEntity;
import org.boot.sensitive.util.DataUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created by chendw on 2023/5/9 17:34.
 */
@RestController
public class TestController {

    @RequestMapping("/sensitive")
    public TestEntity sensitive() {
        return DataUtils.getEntity();
    }
}
