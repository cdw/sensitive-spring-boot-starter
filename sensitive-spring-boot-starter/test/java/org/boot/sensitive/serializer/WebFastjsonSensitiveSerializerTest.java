package org.boot.sensitive.serializer;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.boot.sensitive.TestApplication;
import org.boot.sensitive.strategy.IStrategy;
import org.boot.sensitive.strategy.StrategyManager;
import org.boot.sensitive.strategy.loader.ISensitiveStrategyLoader;
import org.boot.sensitive.util.SensitiveTestUtil;
import org.boot.sensitive.web.TestController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK
)
@ContextConfiguration(classes = {TestApplication.class})
@AutoConfigureMockMvc
class WebFastjsonSensitiveSerializerTest {

    @Autowired
    TestController controller;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired(required = false)
    List<IStrategy> strategyList;
    @Autowired
    ISensitiveStrategyLoader loader;

    @Autowired
    StrategyManager strategyManager;
    @Autowired
    MockMvc mvc;

    private FastJsonHttpMessageConverter fastJsonHttpMessageConverter;
    @Autowired
    private FastjsonSensitiveValueFilter fastjsonSensitiveValueFilter;


    public FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
        //1、定义一个convert转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        //2、添加fastjson的配置信息
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        //升级最新版本需加=============================================================
        List<MediaType> supportedMediaTypes = new ArrayList<>();
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
        supportedMediaTypes.add(MediaType.APPLICATION_ATOM_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
        supportedMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
        supportedMediaTypes.add(MediaType.APPLICATION_PDF);
        supportedMediaTypes.add(MediaType.APPLICATION_RSS_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_XHTML_XML);
        supportedMediaTypes.add(MediaType.APPLICATION_XML);
        supportedMediaTypes.add(MediaType.IMAGE_GIF);
        supportedMediaTypes.add(MediaType.IMAGE_JPEG);
        supportedMediaTypes.add(MediaType.IMAGE_PNG);
        supportedMediaTypes.add(MediaType.TEXT_EVENT_STREAM);
        supportedMediaTypes.add(MediaType.TEXT_HTML);
        supportedMediaTypes.add(MediaType.TEXT_MARKDOWN);
        supportedMediaTypes.add(MediaType.TEXT_PLAIN);
        supportedMediaTypes.add(MediaType.TEXT_XML);
        fastConverter.setSupportedMediaTypes(supportedMediaTypes);
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        //3、在convert中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);
        //4、将convert添加到converters中
        return fastConverter;
    }

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        fastJsonHttpMessageConverter = fastJsonHttpMessageConverter();
        fastJsonHttpMessageConverter.getFastJsonConfig().setSerializeFilters(fastjsonSensitiveValueFilter);
        mvc = MockMvcBuilders.standaloneSetup(controller)
                .setMessageConverters(fastJsonHttpMessageConverter).build();

        SensitiveTestUtil.initConfig(strategyManager);
    }

    @Test
    void serializer() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/sensitive")
                .characterEncoding(StandardCharsets.UTF_8.name());

        ResultActions resultActions = mvc.perform(builder);
        resultActions.andReturn().getResponse().setCharacterEncoding("UTF-8");
        resultActions.andDo(print());

    }
}