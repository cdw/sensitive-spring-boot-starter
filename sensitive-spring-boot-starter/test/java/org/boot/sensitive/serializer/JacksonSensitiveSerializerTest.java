package org.boot.sensitive.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.boot.sensitive.TestApplication;
import org.boot.sensitive.strategy.IStrategy;
import org.boot.sensitive.strategy.StrategyManager;
import org.boot.sensitive.strategy.loader.ISensitiveStrategyLoader;
import org.boot.sensitive.util.DataUtils;
import org.boot.sensitive.util.SensitiveTestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@ContextConfiguration(classes = {TestApplication.class})
class JacksonSensitiveSerializerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired(required = false)
    List<IStrategy> strategyList;
    @Autowired
    ISensitiveStrategyLoader loader;

    @Autowired
    private StrategyManager strategyManager;


    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        SensitiveTestUtil.initConfig(strategyManager);
    }

    @Test
    void serializer() throws JsonProcessingException {
        TestEntity entity = DataUtils.getEntity();
        String result = objectMapper.writeValueAsString(entity);
        log.info("result: {}", result);

        strategyManager.addConfig("org.boot.sensitive.serializer.TestEntity#name", "chineseName");
        strategyManager.reload();
        result = objectMapper.writeValueAsString(entity);
        log.info("result: {}", result);
    }


    @Test
    void serializerList() throws JsonProcessingException {
        List<TestEntity> entitys = getEntitys();
        String result = objectMapper.writeValueAsString(entitys);
        log.info("result: {}", result);
    }

    private List<TestEntity> getEntitys() {
        List<TestEntity> list = new ArrayList<>();
        list.add(DataUtils.getEntity());
        list.add(DataUtils.getEntity());
        return list;
    }

}