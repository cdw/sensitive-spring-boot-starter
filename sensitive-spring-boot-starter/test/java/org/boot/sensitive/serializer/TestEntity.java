package org.boot.sensitive.serializer;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Created by chendw on 2023/5/6 9:21.
 */
@Setter
@Getter
public class TestEntity extends TestParentEntity {
    private String name;
    private String phone;
    private String workPhone;
    private String idCard;
    private int age;
    private String password;
    private String cardNo;
    private String address;
    private List<String> list = new ArrayList<>();
}
