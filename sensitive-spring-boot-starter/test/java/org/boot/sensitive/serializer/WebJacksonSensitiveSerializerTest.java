package org.boot.sensitive.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.boot.sensitive.TestApplication;
import org.boot.sensitive.strategy.IStrategy;
import org.boot.sensitive.strategy.StrategyManager;
import org.boot.sensitive.strategy.loader.ISensitiveStrategyLoader;
import org.boot.sensitive.util.SensitiveTestUtil;
import org.boot.sensitive.web.TestController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK
)
@ContextConfiguration(classes = {TestApplication.class})
@AutoConfigureMockMvc
class WebJacksonSensitiveSerializerTest {

    @Autowired
    TestController controller;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired(required = false)
    List<IStrategy> strategyList;
    @Autowired
    ISensitiveStrategyLoader loader;

    @Autowired
    StrategyManager strategyManager;
    @Autowired
    MockMvc mvc;

    @Autowired
    private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {

        mvc = MockMvcBuilders.standaloneSetup(controller)
                .setMessageConverters(mappingJackson2HttpMessageConverter).build();

        SensitiveTestUtil.initConfig(strategyManager);
    }

    @Test
    void serializer() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/sensitive")
                .characterEncoding(StandardCharsets.UTF_8.name());

        ResultActions resultActions = mvc.perform(builder);
        resultActions.andReturn().getResponse().setCharacterEncoding("UTF-8");
        resultActions.andDo(print());

    }
}