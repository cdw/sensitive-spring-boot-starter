package org.boot.sensitive.serializer;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.boot.sensitive.TestApplication;
import org.boot.sensitive.strategy.IStrategy;
import org.boot.sensitive.strategy.StrategyManager;
import org.boot.sensitive.strategy.loader.ISensitiveStrategyLoader;
import org.boot.sensitive.util.DataUtils;
import org.boot.sensitive.util.SensitiveTestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

@Slf4j
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@ContextConfiguration(classes = {TestApplication.class})
class FastjsonSensitiveSerializerTest {

    @Autowired(required = false)
    List<IStrategy> strategyList;
    @Autowired
    ISensitiveStrategyLoader loader;

    @Autowired
    private StrategyManager strategyManager;
    @Autowired
    private SensitiveSerializer sensitiveSerializer;

    private FastjsonSensitiveValueFilter valueFilter;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        valueFilter = new FastjsonSensitiveValueFilter(sensitiveSerializer);

        SensitiveTestUtil.initConfig(strategyManager);
    }

    @Test
    void serializer() {
        TestEntity entity = DataUtils.getEntity();
        String result = JSON.toJSONString(entity, valueFilter);
        log.info("result: {}", result);

        strategyManager.addConfig("org.boot.sensitive.serializer.TestEntity#name", "chineseName");
        strategyManager.reload();
        result = JSON.toJSONString(entity, valueFilter);
        log.info("result: {}", result);
    }

}