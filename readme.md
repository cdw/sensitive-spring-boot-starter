# SpringBoot通用数据脱敏组件

<p>
  <a href="https://central.sonatype.com/search?q=io.gitee.cdw%3Asensitive-spring-boot-starter">
    <img alt="maven" src="https://img.shields.io/maven-central/v/io.gitee.cdw/sensitive-spring-boot-starter.svg?style=flat-square">
  </a>
  <a href="https://www.apache.org/licenses/LICENSE-2.0">
    <img alt="code style" src="https://img.shields.io/badge/license-Apache%202-4EB1BA.svg?style=flat-square">
  </a>
</p>

> 网上有很多的脱敏组件，但普遍都是基于注解通过硬编码的方式来实现脱敏，不够灵活，本项目主要是将脱敏规则与注解剥离，可以通过读取数据库脱敏规则，并且随时可以调整脱敏策略。
> 
## 引入依赖

> `sensitive-spring-boot-starter`为自己封装的组件，已发布到`maven central repository`仓库

```xml
<dependency>
   <groupId>io.gitee.cdw</groupId>
   <artifactId>sensitive-spring-boot-starter</artifactId>
   <version>版本号</version>
</dependency>
```


## 加载脱敏策略
> 实现脱敏策略加载接口`ISensitiveStrategyLoader`,并注册成SpringBean(以下模拟从数据库加载脱敏策略)

```java
@Component
public class SensitiveStrategyLoader implements ISensitiveStrategyLoader {
   @Override
   public List<FieldStrategyConfig> load() {
        // 模拟从数据库加载脱敏规则
        List<FieldStrategyConfig> list = new ArrayList<>();
        
        // 脱敏规则 123456 -> ******
        FieldStrategyConfig config2 = new FieldStrategyConfig();
        config2.setName("/sensitive"); // SpringMvc使用接口Uri作为接口名称
        config2.setField("com.example.entity.SensitiveInfo#password");
        config2.setType("all"); // 使用内置的脱敏策略
        list.add(config2);

        // 脱敏规则 330327111111111994 -> 330327********1994
        FieldStrategyConfig config3 = new FieldStrategyConfig();
        config3.setField("com.example.entity.SensitiveInfo#idCard");
        config3.setType("idcard"); // 使用内置的脱敏策略
        list.add(config3);

        // 脱敏规则 18011111068 -> *******1068
        FieldStrategyConfig config4 = new FieldStrategyConfig();
        config4.setField("com.example.entity.SensitiveInfo#workMobile"); // 不指定接口名称则对所有接口生效
        config4.setType("custom"); // 使用自定义的脱敏策略
        config4.setLeft(0); // 左边显示0位
        config4.setRight(4); // 右边显示4位
        list.add(config4);

        // 脱敏规则  123456789 -> 123**6789
        FieldStrategyConfig config5 = new FieldStrategyConfig();
        config5.setField("com.example.entity.SensitiveInfo#cardNo");
        config5.setType("custom"); // 使用自定义的脱敏策略
        config5.setPattern("(?<=\\w{3})\\w(?=\\w{4})"); // 正则脱敏
        list.add(config5);
      return list;
   }
}
```

> 编写测试的Controller
```java
@RestController
public class SensitiveController {

    @RequestMapping("/sensitive")
    public SensitiveInfo sensitive() {
        SensitiveInfo sensitiveInfo = new SensitiveInfo();
        sensitiveInfo.setMobile("13611112400");
        sensitiveInfo.setName("王老五");
        sensitiveInfo.setIdCard("330327111111111994");
        return sensitiveInfo;
    }
}
```

> 返回结果
```json
{
  "name": "王老五",
  "mobile": "136****2400",
  "workMobile": "*******2400",
  "idCard": "330327********1994"
}
```

**动态加载策略**
> 通过使用`StrategyManager`类的以下方法动态添加脱敏策略

```java
public class StrategyManager{
   /**
    * 全局脱敏策略
    *
    * @param field 需要脱敏的类字段格式：类名#成员属性名(如：com.example.entity.SensitiveInfo#Name)
    * @param type  脱敏策略
    * @param left  左边显示位数
    * @param right 右边显示位数
    */
   void addConfig(String field, String type, int left, int right)

   /**
    * 特定接口脱敏策略
    *
    * @param name  接口名称
    * @param field 需要脱敏的类字段 
    * @param type  脱敏策略
    * @param left  左边显示位数
    * @param right 右边显示位数
    */
   void addReqConfig(String name, String field, String type, int left, int right)

   /**
    * 重新加载脱敏策略
    */
   public void reload()  
}
```

> 测试案例

```java

// 通过Spring注入脱敏策略管理器
@Autowired
private StrategyManager strategyManager;

// 添加策略 针对所有接口生效
strategyManager.addConfig("com.example.entity.SensitiveInfo#Name", "chineseName");
// 添加策略 只对/sensitive这个接口生效
strategyManager.addReqConfig("/sensitive","com.example.entity.SensitiveInfo#Name", "chineseName");

// 重新加载脱敏策略使之生效
strategyManager.reload();
```

## 内置脱敏规则
| 策略代码        | 策略名称   | 默认样式               |
|-------------|--------|--------------------|
| idcard      | 证件号码脱敏 | 330327********1994 |
| mobile      | 手机号码脱敏 | 136****2400        |
| chineseName | 中文姓名脱敏 | 王*五                |
| email       | 邮箱脱敏   | c***@gmail.com     |
| all         | 全文脱敏   | ******             |

